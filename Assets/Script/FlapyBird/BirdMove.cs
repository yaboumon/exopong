using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BirdMove : MonoBehaviour
{
    Rigidbody2D rb2d;
    public float speed = 5f;
    [SerializeField]
    private float flapForce = 20f;
    bool isDead;
    public GameObject ReplayButton;
    public GameObject PlayButton;
    int score = 0;
    public Text scoreText;
    public void Unfreeze()
    {
        Time.timeScale = 1;
        PlayButton.SetActive(false);
    }
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0;
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.velocity = Vector2.right * speed * Time.deltaTime;
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        isDead = true;
        rb2d.velocity = Vector2.zero;
        ReplayButton.SetActive(true);
        GetComponent<Animator>().SetBool("isDead", true);
    }

    public void Replay()
    {
        SceneManager.LoadScene(1);
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Score")
        {
            score++;
            Debug.Log("score");
            scoreText.text = score.ToString();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !isDead)
        {
            rb2d.velocity = Vector2.right * speed * Time.deltaTime;

            rb2d.AddForce(Vector2.up * flapForce);

        }
           
    }
   
}
